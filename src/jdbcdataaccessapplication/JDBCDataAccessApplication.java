package jdbcdataaccessapplication;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LaboratorioFISI
 */
public class JDBCDataAccessApplication {
    
    public static void main(String[] args) {
        
        JDBCDataAccessClass jdbc = new JDBCDataAccessClass();
        jdbc.initialize();
        int empCount = jdbc.listStaff();
        System.out.println("El total de empleados es: " + empCount);
    }
}
